#import "macros.asm"
#importonce

// ----------------------------------------------------------------
// Font and charmaps

.struct Font {
    chars,
    tiles,
    width_chars,
    height_chars,
    ascii_offset
}

.const titleFont = Font(
    LoadBinary("assets/font_chars.bin"),
    LoadBinary("assets/font_tiles.bin"),
    4,
    2,
    -$40)

.const textFont = Font(
    LoadBinary("assets/font2_chars.bin"),
    false,
    1,
    1,
    -$40)

*= ram.chars "Char Data"
font_chars:
    .fill titleFont.chars.getSize(), titleFont.chars.get(i) 
font2_chars:
    .fill textFont.chars.getSize(), textFont.chars.get(i) 
font_tiles:
    .fill titleFont.tiles.getSize(), titleFont.tiles.get(i)

// ----------------------------------------------------------------
// Static Text strings

.const titleText1 = asciiRot("DUEL", titleFont.ascii_offset)
.const titleText2 = asciiRot("CHALLENGE", titleFont.ascii_offset)
.const titleText3 = asciiRot(@"BY\$40FALCO\$40HIRSCHENBERGER", textFont.ascii_offset)
.const titleText4 = asciiRot(@"2019", textFont.ascii_offset)
.const lost_text = asciiRot(@"HAS\$40LOST\$40THE\$40DUEL", textFont.ascii_offset)
.const won_text = asciiRot(@"HAS\$40WON\$40THE\$40DUEL", textFont.ascii_offset)


// ----------------------------------------------------------------
// Music and Sfx data

.const titleMusic = LoadSid("assets/Mindfuck.sid")
.const shotSfx = LoadSid("assets/M7_Shot_BASIC.sid")

*= $1000 "Sound Data"
    .fill titleMusic.size, titleMusic.getData(i)
    .fill shotSfx.size, shotSfx.getData(i)

// ----------------------------------------------------------------
// Sprite data

.const beeSprite = LoadBinary("assets/bee_sprite.raw")

*= $3000 "Sprite Data"
bee_sprite: .fill beeSprite.getSize(), beeSprite.get(i)

// ----------------------------------------------------------------
// Game data

// All games return their winner in the two lsb of the zp 'game_result' Only one
// bit will be set at a time
.namespace result {
    .label player1_lost = %00000001
    .label player1_won  = %00000010
    .label player2_lost = %00000100
    .label player2_won  = %00001000
}

.label game_result = zp.f8


*= $5000 "Game Data"
.namespace player1 {
    name: fillAsciiRot(@"ALICE\$40\$40\$40", -$40)
    score: .byte $00

    // right shift key
    .label keyCiaA = %01111111
    .label keyCiaB = %00000001
}

.namespace player2 {
    name: fillAsciiRot(@"BOB\$40\$40\$40\$40\$40", -$40)
    score: .byte $00

    // left shift key
    .label keyCiaA = %01111111
    .label keyCiaB = %00001000
}
