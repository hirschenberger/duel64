# Duel Challenge

*"Challenge your friends on retro hardware"*

This is a small C64 game, developed with *KickAssembler* just for the taste of good old times.  

![](doc/titlescreen.png)

---

copyright 2019, Falco Hirschenberger
