BINDIR=bin
JAVA=java
KICKASS=$(JAVA) -jar ./bin/KickAss.jar
KICKASSFLAGS=-odir $(BINDIR) -log ./bin/buildlog.txt -showmem -debugdump
EXOMIZER=exomizer
EXOMIZERFLAGS=sfx basic
VICE=x64
DEBUGGER=c64debugger
DEBUGGERFLAGS=-layout 10 -debuginfo bin/duel64.dbg -wait 2500 -autojmp 
VICEFLAGS=-sidenginemodel 1803 -keybuf "\88"

ASSETS=assets/font_chars.bin \
	   assets/font_tiles.bin \
	   assets/font2_chars.bin \
	   assets/Mindfuck.sid \
	   assets/M7_Shot_BASIC.sid \
	   assets/bee_sprite.raw

SOURCES=main.asm \
        macros.asm \
		data.asm \
		util.asm \
		title_screen.asm \
		reaction_game.asm \
		countdown_game.asm \
		colorname_game.asm

OBJECTS=$(BINDIR)/duel64.prg
PACKEDOBJECTS=$(BINDIR)/duel64.prg.exo
 
%.prg: main.asm $(SOURCES) $(ASSETS)
	$(KICKASS) $(KICKASSFLAGS) $< -o $@
 
%.prg.exo: $(OBJECTS)
	$(EXOMIZER) $(EXOMIZERFLAGS) $< -o $@
 
all: $(PACKEDOBJECTS)

vice: $(PACKEDOBJECTS)
	$(VICE) $(X64FLAGS) $<

debug: $(PACKEDOBJECTS)
	$(DEBUGGER) $(DEBUGGERFLAGS) -prg $<

.PHONY: clean
clean:
	rm -f $(BINDIR)/*.prg $(BINDIR)/*.exo $(BINDIR)/*.dbg $(BINDIR)/buildlog.txt
