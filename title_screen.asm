#import "macros.asm"
#import "data.asm"
#importonce
.filenamespace title_screen

.label scroll_pos = zp.a16

.const IRQ_DELAY_CYCLES = 4

draw: {
    lda #RED
    sta vic.color.border
    sta vic.color.background0

    lda #$00
    sta scroll_pos
    sta scroll_pos+1
    jsr draw_title

    lda #IRQ_DELAY_CYCLES
    sta zp.irq_delay_nr
        
    lda #titleMusic.startSong-1
    jsr titleMusic.init

    setIrqHandler(irq)
    rts
}

irq: {
    jsr titleMusic.play

    // wait number of interrupts
    dec zp.irq_delay_nr
    bne exit

    // restore default values
    lda #IRQ_DELAY_CYCLES
    sta zp.irq_delay_nr

    lda vic.control2
    and #%11111000
    ora scroll_pos
    sta vic.control2
instruction:
    inc scroll_pos
    lda scroll_pos
    beq up
    cmp #%00000111
    beq down
    jmp instruction2
up:
    lda #$e6                // write inc over dec
    sta instruction
    jmp instruction2
down:
    lda #$c6                // write dec over inc
    sta instruction

instruction2:
    inc scroll_pos+1
    lda scroll_pos+1
    beq up2
    cmp #15
    beq down2
    jmp skip
 up2:
    lda #$e6                // write inc over dec
    sta instruction2
    jmp skip
 down2:
    lda #$c6                // write dec over inc
    sta instruction2

skip:
    lda #%00000111
    and scroll_pos
    sta scroll_pos

    jsr draw_title

exit:
    jmp $ea31
}


draw_title: {
    ldx scroll_pos+1
    .for(var c=0; c<titleText1.size(); c++){
        .for(var y=0; y<2; y++) {
            .for(var x=0; x<4; x++){
                lda font_tiles+titleText1.get(c)*8 + y*4 + x                
                sta screenPos(ram.screen, 2 + c*4 + x, 5 + y),X
                lda #WHITE  
                sta screenPos(ram.color, 2 + c*4 + x, 5 + y),X
            }
        }
    }

    // color gradient
    lda #LIGHT_GREY
    sta screenPos(ram.color, 1, 5),X
    sta screenPos(ram.color, 1, 6),X
    lda #GREY
    sta screenPos(ram.color, 0, 5),X
    sta screenPos(ram.color, 0, 6),X
    lda #BLACK
    sta screenPos(ram.color, -1, 5),X
    sta screenPos(ram.color, -1, 6),X


    // clear chars after the title
    lda #$00
    sta screenPos(ram.screen, 2+titleText1.size()*4, 5),X
    sta screenPos(ram.screen, 2+titleText1.size()*4, 6),X

    .for(var c=0; c<titleText2.size(); c++){
        .for(var y=0; y<2; y++) {
            .for(var x=0; x<4; x++){
                lda font_tiles+titleText2.get(c)*8 + y*4 + x
                sta screenPos(ram.screen, 2 + c*4 + x, 10 + y)
                lda #BLACK
                sta screenPos(ram.color, 2 + c*4 + x, 10 + y)
            }
        }
    }

    .for(var c=0; c<titleText3.size(); c++){
        lda #(titleFont.chars.getSize()/8 + titleText3.get(c))
        sta screenPos(ram.screen, 9+c, 20)
        lda #BLACK
        sta screenPos(ram.color, 9+c, 20)
    }

    .for(var c=0; c<titleText4.size(); c++){
        lda #(titleFont.chars.getSize()/8 + 43 + titleText4.get(c))
        sta screenPos(ram.screen, 18+c, 21)
        lda #BLACK
        sta screenPos(ram.color, 18+c, 21)
    }

    rts
}

