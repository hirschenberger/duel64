#importonce

.namespace kernal {
    .label cls = $e544
    .label main_irq = $ea31
}

.namespace vic {
    .namespace color {
        .label border = $d020
        .label background0 = $d021
        .label background1 = $d022
        .label background2 = $d023
    }
    .label control1 = $d011
    .label rasterline = $d012
    .label control2 = $d016
    .label location = $d018
}

.namespace cia {
    .label portA = $dc00
    .label portB = $dc01
    .label tod_sdiv10 = $dc08
    .label tod_s = $dc09
    .label tod_m = $dc0a
    .label toh_h = $dc0b
    .label control = $dc0d
}

.namespace ram {
    .label screen = $0400
    .label color = $d800
    .label chars = $2000
}

// usable:
// $92 - $96
// $a3 - $b1
// $f7 - $fa
// $fb - $fe
.namespace zp {
    .label a16 = $92
    .label b16 = $94
    .label c16 = $96
    .label d16 = $a3
    .label e16 = $a5
    .label f16 = $a7
    
    .label a8 = $a8
    .label b8 = $a9
    .label c8 = $aa
    .label d8 = $ab
    .label e8 = $ac
    .label f8 = $ad

    .label irq_delay_nr = $ae

    .label hw_irq = $0314
}


.function @screenPos(screen_start, x, y) {
	.return screen_start + y*40 + x;
}

.function @getTextMemory(screenMem, charSet) {
  .return charSet<<1 | screenMem<<4
}

.macro @configTextMemory(screenMem, charSet) {
  lda #getTextMemory(screenMem, charSet)
  sta $d018
}

.macro @drawText(text, charset, x, y) {
    .for(var c=0; c<text.size(); c++) {
        lda #(charset + text.get(c))
        sta screenPos(ram.screen, x+c, y)
    }
}

.macro @textAlignRight(string, size) {
loop:
  ldx #size-1
  lda string,X
  bne end
copy:
  dex
  lda string,X             // shift copy right
  sta string+1,X
  lda #$00                 // fill with 0 left
  sta string,X
  txa
  bne copy
  jmp loop
end:
}

.macro @drawTextHCentered(text, charset, y) {
  drawText(text, charset, 20 - text.size()/2, y)
}

.macro @drawTextLeft(text, charset, xoffset, y) {
  drawText(text, charset, xoffset, y)
}

.macro @drawTextRight(text, charset, xoffset, y) {
  drawText(text, charset, 40 - text.size() - xoffset, y)
}

.macro @drawDynText(text, text_size, charset_offset, x, y) {
  ldx #text_size-1
loop:
  clc
  lda text,X
  adc #charset_offset
  sta screenPos(ram.screen, x, y),X
  dex
  bpl loop
}

.macro resetColorRam(color) {
  ldx #$ff
  lda #color
  .for(var i=0; i<4; i++) {
    sta ram.color+i*$ff,X
    dex
    bne *-4
  }
}

.macro drawScore(score, charset_offset, x, y) {
  lda score
  asl
  tax
  lda div10table,x
  adc #charset_offset
  sta screenPos(ram.screen, x, y)
  lda div10table+1,x
  adc #charset_offset
  sta screenPos(ram.screen, x+1, y)
}

div10table:
    .fill $ff, [i/10, mod(i, 10)]

.macro @setIrqHandler(function) {
    sei
    lda #<function
    sta zp.hw_irq
    lda #>function
    sta zp.hw_irq+1
    cli
}

.function @neg(value) {
	.return value ^ $FF
}

.assert "neg($00) gives $FF", neg($00), $FF
.assert "neg($FF) gives $00", neg($FF), $00
.assert "neg(%10101010) gives %01010101", neg(%10101010), %01010101

.function @asciiRot(text, offset) {
    .var out = List()
    .for(var i=0; i<text.size(); i++) {
        .eval out.add(text.charAt(i) + offset)
    }
    .return out
}

.assert "asciiRot rotates the characters of the string 'AAA' by an offset 1", asciiRot("AAA", 1), List().add($42,$42,$42)

.macro @fillAsciiRot(text, offset) {
  // thank you Alex Goldblat!
  .fill text.size(), text.charAt(i) + offset
}

.assert "fillAsciiRot", { fillAsciiRot("ab", 1)} , { .text "bc" }
//.assert "fillAsciiRot", { fillAsciiRot("bc", -1)} , { .text "ab" }
