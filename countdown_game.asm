#import "macros.asm"
#import "util.asm"
#import "data.asm"
#importonce
.filenamespace countdown_game

.const shootout_desc = asciiRot(@"PRESS\$40BUTTON\$40WHEN\$40TIMED\$40OUT", textFont.ascii_offset)

.label old_time = zp.a8
.label timeout_register = zp.c8

.const TIMEOUT = 9
.const DIGIT_X = 18
.const DIGIT_Y = 10

start: {
    drawTextHCentered(shootout_desc, font_chars + titleFont.chars.getSize()/8, 1)
    jsr draw_scores
    
    lda #BLACK  
    sta vic.color.background0 
    sta vic.color.border

    // diable tod latch
    lda #$01
    sta cia.tod_sdiv10

    lda #TIMEOUT
    sta timeout_register

    // only react on player's keys
    lda #player1.keyCiaA | player2.keyCiaA
    sta cia.portA
    jsr draw_digit

wait:
    lda cia.tod_s
    tax
    cmp old_time
    bne update_time
    jmp wait_input

update_time:
    stx old_time
    dec timeout_register

    // skip drawing when < 4
    lda timeout_register
    cmp #4
    bcc hide_digit
    jsr draw_digit
    jmp check_timeout

hide_digit:
    jsr remove_digit

check_timeout:
    lda timeout_register
    beq timeout

wait_input:
    // check for player's keypress
    lda cia.portB
    and #player1.keyCiaB
    beq player1_lost
    lda cia.portB
    and #player2.keyCiaB
    beq player2_lost
    jmp wait

player1_lost:
    lda #result.player1_lost
    sta game_result
    rts

player2_lost:
    lda #result.player2_lost
    sta game_result
    rts

timeout:
    // check and loop for player's keypress
    lda cia.portB
    and #player1.keyCiaB
    beq player1_won
    lda cia.portB
    and #player2.keyCiaB
    beq player2_won
    jmp timeout

player1_won:
    lda #result.player1_won
    sta game_result
    rts

player2_won:
    lda #result.player2_won
    sta game_result
    rts
}

draw_digit: {
    .const font_digits_offset = 27*8
    ldy timeout_register
    ldx mul8_table,Y
    .for(var y=0; y<2; y++) {
        .for(var x=0; x<4; x++){
            lda font_tiles+font_digits_offset + y*4 + x, X                
            sta screenPos(ram.screen, DIGIT_X + x, DIGIT_Y + y)
            lda #WHITE  
            sta screenPos(ram.color, DIGIT_X + x, DIGIT_Y + y)
        }
    }
    rts
}

remove_digit: {
    .for(var y=0; y<2; y++) {
        .for(var x=0; x<4; x++){
            lda #$00        
            sta screenPos(ram.screen, DIGIT_X + x, DIGIT_Y + y)
            lda #WHITE  
            sta screenPos(ram.color, DIGIT_X + x, DIGIT_Y + y)
        }
    }
    rts
}

mul8_table:
    .fill $ff, i*8
