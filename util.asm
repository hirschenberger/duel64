#import "macros.asm"
#importonce
.filenamespace util

wait_for_space_key: {
    lda #%01111111     // only react on keyboard row 1
    sta cia.portA
    lda cia.portB
    cmp #%11101111     // is the space key down?
    bne wait_for_space_key
    rts
}

// 'A' will contain a random byte
random_byte: {
    // use timer registers to assemble a random byte
    lda $dc04  
    eor $dc05  
    eor $dd04  
    adc $dd05  
    eor $dd06  
    eor $dd07 
    rts
}

reset_screen: {
    // restore default irq handler
    setIrqHandler(kernal.main_irq)

    ldx #$00
    txa
loop:
    .for(var n=0; n<4; n++) {
        sta ram.screen + n*250, X
    }
    inx
    bne loop
    rts
}