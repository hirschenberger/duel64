BasicUpstart2(main)

#import "macros.asm"
#import "data.asm"
#import "util.asm"
#import "title_screen.asm"
#import "reaction_game.asm"
#import "countdown_game.asm"


main: {
    configTextMemory(1, 4)
    jsr util.reset_screen
    jsr title_screen.draw
    jsr util.wait_for_space_key
next_game:
    .for(var i=0; i<5; i++){
        jsr util.reset_screen
        jsr reaction_game.start
        jsr draw_result
        jsr util.wait_for_space_key
    }
    .for(var i=0; i<5; i++){
        jsr util.reset_screen
        jsr countdown_game.start
        jsr draw_result
        jsr util.wait_for_space_key
    }
    jmp next_game
}

draw_result: {
    jsr util.reset_screen
    lda game_result
    tax
    tay
    cmp #result.player1_lost
    beq player1_lost
    cpx #result.player1_won
    beq player1_won
    cpy #result.player2_lost
    beq player2_lost 
    jmp player2_won

player1_lost: 
    drawDynText(player1.name, 8, titleFont.chars.getSize()/8, 17, 10)
    inc player2.score
    jmp lost
    
player2_lost: 
    drawDynText(player2.name, 8, titleFont.chars.getSize()/8, 13, 10)
    inc player1.score
    jmp lost

player1_won: 
    drawDynText(player1.name, 8, titleFont.chars.getSize()/8, 17, 10)
    inc player1.score
    jmp won

player2_won:
    drawDynText(player2.name, 8, titleFont.chars.getSize()/8, 13, 10)
    inc player2.score
    jmp won

lost: 
    drawTextHCentered(lost_text, font_chars + titleFont.chars.getSize()/8, 12)
    jsr draw_scores
    resetColorRam(BLACK)
    lda #RED
    sta vic.color.background0 
    sta vic.color.border
    jmp exit

won:
    drawTextHCentered(won_text, font_chars + titleFont.chars.getSize()/8, 12)
    jsr draw_scores
    resetColorRam(BLACK)
    lda #GREEN
    sta vic.color.background0 
    sta vic.color.border
    
exit:
    rts
}

draw_scores: {
    resetColorRam(WHITE)
    drawDynText(player1.name, 8, titleFont.chars.getSize()/8, 1, 23)
    textAlignRight(player2.name, 8)
    drawDynText(player2.name, 8, titleFont.chars.getSize()/8, 40 - 9, 23)
    drawScore(player1.score, titleFont.chars.getSize()/8+27, 1, 24)
    drawScore(player2.score, titleFont.chars.getSize()/8+27, 40-3, 24)
    rts
}



