#import "macros.asm"
#import "util.asm"
#import "data.asm"
#importonce
.filenamespace reaction_game

.const shootout_desc = asciiRot(@"PRESS\$40BUTTON\$40WHEN\$40BACKGROUND\$40WHITE", textFont.ascii_offset)

start: {
    drawTextHCentered(shootout_desc, font_chars + titleFont.chars.getSize()/8, 1)
    jsr draw_scores
    
    lda #BLACK  
    sta vic.color.background0 
    sta vic.color.border

    // 16bit delay count 
    jsr util.random_byte
    sta zp.a16
    jsr util.random_byte

    // we have a loop speed of 60/sec because of the rasterline
    // that's 4400 ms overflow for the low byte.
    // mask out the higest bytes to limit max time (17,6s)
    and #%00000011
    // set the low bit to have at least 4400ms
    ora #1
    sta zp.a16+1

    // only react on player's keys
    lda #player1.keyCiaA | player2.keyCiaA
    sta cia.portA

wait:
    //.break
    // check for player's keypress
    lda cia.portB
    and #player1.keyCiaB
    beq player1_lost

    lda cia.portB
    and #player2.keyCiaB
    beq player2_lost

    lda vic.rasterline
    cmp #$fb
    bne wait

    sec
    lda zp.a16
    sbc #1
    sta zp.a16
    lda zp.a16+1
    sbc #0
    sta zp.a16+1
    bne wait
    lda zp.a16
    bne wait
    jmp timeout

player1_lost:
    lda #result.player1_lost
    sta game_result
    rts

player2_lost:
    lda #result.player2_lost
    sta game_result
    rts

timeout:
    resetColorRam(BLACK)
    lda #WHITE
    sta vic.color.background0 
    sta vic.color.border

    // check and loop for player's keypress
    lda cia.portB
    and #player1.keyCiaB
    beq player1_won
    lda cia.portB
    and #player2.keyCiaB
    beq player2_won
    jmp timeout

player1_won:
    lda #result.player1_won
    sta game_result
    rts

player2_won:
    lda #result.player2_won
    sta game_result
    rts
}

